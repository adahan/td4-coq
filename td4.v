Require Import Program Bool Arith List.
Import ListNotations.

Fixpoint exp2 (n : nat) := match n with
                           |O => 1
                           |S n => 2*(exp2 n)
                           end.


Module My_list.
  Inductive list (A : Type) : Type :=
  |nil : list A
  |cons : A -> list A -> list A.
  Arguments nil {A}.
  Arguments cons {A}.

  Definition decons {A} (l : list A) := match l with (* Complexité : O(1) *)
                                        |nil => None
                                        |cons hd tl => Some(hd,tl)
                                        end.

  Fixpoint nth {A} (l : list A) (n : nat) := match decons l with (* Complexité : O(n) *)
                                             |None => None
                                             |Some (hd,tl) => match n with
                                                              |O => Some hd
                                                              |S k => nth tl k
                                                              end
                                             end.
End My_list.

Module B_list.

  Fixpoint pbTree (A : Type) n : Type := match n with
                                         |O => A
                                         |S n => (pbTree A n * pbTree A n)%type
                                         end.

  Inductive auxblist (A : Type) (n : nat) : Type :=
  |Nil : auxblist A n
  |Cons k : (pbTree A n) -> (auxblist A (S (k+n))) -> auxblist A n.
  Arguments Nil {A} {n}.
  Arguments Cons {A} {n}.

  Inductive blist (A : Type) :=
  |Start k : auxblist A k -> blist A.
  Arguments Start {A}.

  Definition empty {A} : (blist A) := Start O Nil.

  Program Fixpoint aux_cons (A : Type) (k : nat) (acc : pbTree A k) (bl : auxblist A k) :
    (blist A) :=
    match bl with
    |Nil => Start k (Cons O acc Nil)
    |Cons O bt nbl => aux_cons A (S k) ((acc,bt) : pbTree A (S k)) nbl
    |Cons (S i) bt nbl => Start (S k) (Cons i ((acc,bt) : pbTree A (S k)) nbl)
    end.

  Program Definition cons {A} (a : A) (bl : blist A) : blist A := (* O(log n) *)
    match bl with
    |Start O abl => aux_cons A O a abl
    |Start (S k) abl => Start O (Cons k (a : pbTree A O) abl)
    end.

  Fixpoint explode {A} (n : nat) : (pbTree A n) -> (auxblist A n) -> (A*blist A):=
    match n with
    |O => fun bt acc => (bt, (Start O acc))
    |S k => fun bt acc => let (bt1,bt2) := bt in explode k bt1 (Cons O bt2 acc)
    end.
  Check explode.
  Program Definition decons {A} (bl : blist A) := (* O(log n) *)
    match bl with
    |Start _ Nil => None
    |Start O (Cons k bt nbl) => Some (bt, Start (S k) nbl)
    |Start (S i) (Cons k bt nbl) => match bt with
                                    |(bt1,bt2) => Some (explode i bt1 (Cons (S k) bt2 nbl))
                                    end
    end.

  Fixpoint bt_nth {A} {k} : (pbTree A k) -> nat -> option A :=
    (* Precondition : n < 2^k *)
    match k with
    |O => fun a n => match n with
                     |O => Some a
                     |_ => None
                     end
    |S k' => fun bt n => let (bt1,bt2) := bt in
                         if n <? (exp2 k')
                         then bt_nth bt1 n
                         else bt_nth bt2 (n - (exp2 k'))
    end.

  Fixpoint aux_nth {A} {k} (bl : auxblist A k) (n : nat) := (* O(log n) *)
    match bl with
    |Nil => None
    |Cons i bt bl' => if (n <? exp2 k)
                      then bt_nth bt n
                      else aux_nth bl' (n - (exp2 k))
    end.

  Definition nth {A} (bl : blist A) (n : nat) := match bl with
                                                 |Start _ abl => aux_nth abl n
                                                 end.
End B_list.

(* Qb-Listes *)
Module QB_list.
  Fixpoint aux_decomp (n : nat) (k : nat) (acc : list nat)   :=
    (* Precondition : n <= 2^k-1 *)
    match k with
    |O => acc
    |1 => match n with
          |O => acc
          |1 => k::acc
          |2 => k::k::acc
          |S k => k::k::acc (* This never happens... I need to know how to prove *)
          end
    |S k' => let pk := exp2 k' -1 in
             if n =? (2 * pk)
             then k'::k'::acc
             else if pk <=? n
                  then aux_decomp (n - pk) k' (k'::acc)
                  else aux_decomp n k' acc
    end.

  Definition decomp (n : nat) := aux_decomp n n [].
  Compute (decomp 11).
  Fixpoint back_to_nat (l : list nat) :=
    match l with
    |[] => 0
    |hd::tl => (exp2 hd - 1) + (back_to_nat tl)
    end.

  Definition next (l : list nat) :=
    match l with
    |[]|O::[] => [1]
    |i::[] => 1::i::[]
    |O::tl => tl (* Never happens *)
    |i::j::tl => if (i <? j)
                 then 1::i::j::tl
                 else (S i)::tl
    end.



  Definition pred (l : list nat) :=
    match l with
    |[]|O::[] => []
    |O::tl => tl (* Never happens *)
    |(S k)::tl => match k with
                  |O => tl
                  |S k' => k::k::tl
                  end
    end.

(* Actual qb-listes *)

  Inductive qbTree (A : Type) : nat -> Type :=
  |Leaf : A -> qbTree A 1 (* notice that there are no constructors for a qbTree A O *)
  |Node n : A -> qbTree A n -> qbTree A n -> qbTree A (S n).
  Arguments Leaf {A}.
  Arguments Node {A} {n}.

  Lemma no_zero_qbt : forall A B, (qbTree A O) -> B.
  Proof.
    intros A B hyp.
    inversion hyp.
  Qed.

  Inductive auxqblist (A : Type) (n : nat) : Type :=
  |QNil : auxqblist A n
  |QCons k : (qbTree A n) -> (auxqblist A (n + (S k))) -> auxqblist A n.
  Arguments QNil {A} {n}.
  Arguments QCons {A} {n}.

  Inductive qblist (A : Type) : Type :=
  |QStart n : auxqblist A n -> qblist A
  |QLoop n : (qbTree A n) -> (auxqblist A n) -> qblist A.
  Arguments QStart {A}.
  Arguments QLoop {A}.

  Definition qb_empty {A} : qblist A := QStart 1 QNil.

  Lemma suc_add : (forall n, S n = n + 1).
    intro n.
    elim n.
    simpl.
    exact eq_refl.
    intros n0 ind_hyp.
    simpl.
    rewrite <- ind_hyp.
    exact eq_refl.
  Qed.


  Program Definition qb_cons {A} (a : A) (qbl : qblist A) := (* O(1) *)
    match qbl with
    |QStart O aqbl => QStart 1 (QCons O (Leaf a) QNil)    (* Both those cases should not happen *)
    |QLoop 0 pbt aqbl => QStart 1 (QCons O (Leaf a) QNil) (* and are treated as an empty qblist *)
    |QStart _ QNil => QStart 1 (QCons 0 (Leaf a) QNil)
    |QLoop 1 qbt QNil => QLoop 1 (Leaf a) (QCons O qbt QNil)
    |QLoop (S (S k)) qbt QNil => QStart 1 (QCons k (Leaf a) (QCons O qbt QNil))
    |QStart 1 aqbl => QLoop 1 (Leaf a) aqbl
    |QStart (S (S k)) aqbl => QStart 1 (QCons k (Leaf a) aqbl)
    |QLoop (S k) pbt (QCons O pbt' aqbl) => QLoop (S (S k)) (Node a pbt pbt') aqbl
    |QLoop (S k) pbt (QCons (S n) pbt' aqbl)  => QStart (S (S k)) (QCons n (Node a pbt pbt') aqbl)
    end.
  Next Obligation of qb_cons.
    rewrite <- (suc_add k) ; exact eq_refl. Defined.


  Program Definition qb_decons {A} (qbl : qblist A) := (* O(1) *)
    match qbl with
    |QStart O _ => None
    |QLoop O _ _ => None
    |QStart (S n) aqbl => match aqbl with
                          |QNil => None
                          |QCons k qbt aqbl' =>
                           match qbt with
                           |Leaf a => Some (a,QStart (S n + S k) aqbl')
                           |Node a bt1 bt2 => Some (a,QLoop n bt1 (QCons (S k) bt2 aqbl'))
                           end
                          end
    |QLoop (S n) qbt aqbl => match qbt with
                             |Leaf a => Some (a,QStart (S n) aqbl)
                             |Node a bt1 bt2 => Some (a, QLoop n bt1 (QCons O bt2 aqbl))
                             end
    end.
  Next Obligation of qb_decons.
    exact (suc_add n).
  Defined.

  Program Fixpoint qb_explode {A} (k : nat) (qbt : qbTree A (S k)) (i : nat) : option A :=
    match i with
    |O => match qbt with
          |Node a _ _ => Some a
          |Leaf a => Some a
          end
    |S i => match qbt with
            |Leaf _ => None (* This wont happen *)
            |Node a qb1 qb2 => match k with 
                               |O => no_zero_qbt A (option A) qb1
                               |S k' => if i<? exp2 k
                                        then qb_explode k' qb1 i
                                        else qb_explode k' qb2 (i - exp2 k)
                               end
            end
    end.

  Program Fixpoint aux_qb_nth {A} {k} (aqbl : auxqblist A k) (n : nat) :=
    match aqbl with
    |QNil => None
    |QCons i qbt aqbl' => match k with
                          |O => no_zero_qbt A (option A) qbt
                          |S k => if n <? (exp2 (S k) - 1)
                                  then qb_explode k qbt n
                                  else aux_qb_nth aqbl' (S n - (exp2 (S k)))
                          end
    end.

  Program Definition  qb_nth {A} (qbl : qblist A) (n : nat) := (* O(log n) *)
    match qbl with
    |QStart k aqbl => aux_qb_nth aqbl n
    |QLoop k qbt aqbl => match k with
                         |O => no_zero_qbt A (option A) qbt
                         |S k => if (n <? (exp2 k) -1)
                                 then qb_explode k qbt n
                                 else aux_qb_nth aqbl ((S n) - exp2 k)
                         end
    end.
(*
Compute (qb_nth (qb_cons 0 (qb_cons 1 (qb_cons 2 qb_empty))) 9).
Fixpoint qbt_drop {A} {k} (n i : nat) (qbt : qbTree A k) (aux : auxqblist A (k + i)) :=
  match k with
  |O => 
Fixpoint aux_drop {A} {k} (n : nat) (aqbl : auxqblist A k) := 
  match aqbl with
  |QNil => QNil
  |QCons i qbt aqbl' => if n <? (exp2 k) -1
                        then qbt_drop n qbt aqbl'
                        else aux_drop ((S n) - exp2 k) aqbl'

*)

End QB_list.
