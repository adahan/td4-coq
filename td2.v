Require Import Arith.
Open Scope bool_scope.

Fixpoint addition (n : nat) m := match m with
                                 |O => n
                                 |S k => S (addition n k)
                                 end.
Fixpoint multiplication (n : nat) m := match m with
                                       |O => O
                                       |S k => addition (multiplication n k) n
                                       end.

Fixpoint soustraction n m := match n with
                             |O => O
                             |S n' => match m with
                                      |O => n
                                      |S m' => soustraction n' m'
                                      end
                             end.

Fixpoint factorielle n := match n with
                          |O => S O
                          |S n' => multiplication (factorielle n') n
                          end.

Fixpoint puissance a n := match n with
                          |O => 1
                          |S n' => multiplication a (puissance a n')
                          end.
Definition egal a b := match (soustraction a b) with
                       |O => match (soustraction b a) with
                             |O => true
                             |_ => false
                             end
                       |_ => false
                       end.

Fixpoint div d n a := match a with
                      |O => false
                      |S k => (egal (multiplication k d) n) || (div d n k)
                      end.
Fixpoint reste a d r := if (div d a (S a)) then r else match a with
                                                       |O => r
                                                       |S a' => reste a' d (S r)
                                                       end.
Compute reste 15 10 1.

Fixpoint pgcd a b n:= match n with
                      |O => O
                      |S n' => match (soustraction a b) with
                               |O => match (reste b a 1) with (* b>=a *)
                                     |O => O
                                     |S O => a
                                     |S k => pgcd b k n'
                                     end
                               |_ => match (reste a b 1) with (* a>b *)
                                     |O => O
                                     |S O => b
                                     |S k => pgcd a k n'
                                     end
                               end
                      end.

Compute pgcd 132 144 100.
Fixpoint fib n := match n with
                  |O => O
                  |1 => 1
                  |S (S k as k') => addition (fib k) (fib k')
                  end.

Fixpoint fastfib n := match n with
                      |O => pair 1 0
                      |S k => match fastfib k with
                              |(p1,p2) => pair (addition p1 p2) p1
                              end
                      end.

Fixpoint fastfastfib n p1 p2 := match n with
                                  |O => p1
                                  |S k => fastfastfib k (addition p1 p2) p1
                                end.

Require Import NArith.
Print N.
