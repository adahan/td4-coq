Require Import Bool Arith  List.
Import ListNotations.

Fixpoint length {A} (l : list A) := match l with
                                    |nil => 0
                                    |cons hd tl => S (length tl)
                                    end.

Fixpoint concatenation {A} (l1 : list A) (l2 : list A) := match l1 with
                                                          |nil => l2
                                                          |cons hd tl => cons hd (concatenation tl l2)
                                                          end.

Fixpoint aux_renversement {A} (l : list A) (acc : list A) :=
  match l with
  |nil => acc
  |cons hd tl => aux_renversement tl (cons hd acc)
  end.

Fixpoint renversement {A} (l : list A) := aux_renversement l nil.

Fixpoint my_map {A} {B} (f : A -> B) (l : list A) := match l with
                                                  |nil => nil
                                                  |cons hd tl => cons (f hd) (map f tl)
                                                  end.

Fixpoint my_filter {A} (f : A -> bool) (l : list A) :=
  match l with
  |nil => nil
  |hd::tl => if (f hd)
             then hd::(filter f tl)
             else (filter f tl)
  end.

Fixpoint my_fold_left {A} {B} (f : A -> B -> A) (a : A) (l : list B) :=
  match l with
  |nil => a
  |hd::tl => my_fold_left f (f a hd) tl
  end.

Fixpoint my_fold_right {A} {B} (f : A -> B -> B) (l : list A) (b : B) :=
  match l with
  |[] => b
  |hd::tl => f hd (my_fold_right f tl b)
  end.

Fixpoint seq (a : nat) (n : nat) := match n with
                                    |O => nil
                                    |S n' => cons a (seq (S a) n')
                                    end.

Fixpoint head {A} (l : list A) := match l with
                                  |nil => None
                                  |hd::tl => Some hd
                                  end.

Fixpoint last {A} (l : list A) := match l with
                                  |nil => None
                                  |hd::[] => Some hd
                                  |hd::tl => last tl
                                  end.

Fixpoint nth {A} (l : list A) (n : nat) := match l,n with
                                           |[],_ => None
                                           |hd::tl,O => Some hd
                                           |hd::tl, S n' => nth tl n'
                                           end.

Fixpoint forallb {A} (f : A -> bool) (l : list A) := match l with
                                                     |[] => true
                                                     |hd::tl => if (f hd)
                                                                then forallb f tl
                                                                else false
                                                     end.

Fixpoint increasing (l : list nat) := match l with
                                      |[]|_::[] => true
                                      |hd1::(hd2::l' as tl) => if hd1 <? hd2 
                                                               then increasing tl
                                                               else false
                                      end.

Fixpoint delta (l : list nat) (k : nat) := 
  match l with
  |[]|_::[] => true
  |hd1::(hd2::l' as tl) => if k <=? max (hd1 - hd2) (hd2 - hd1) 
                           then delta tl k
                           else false
  end.

Fixpoint aux_split {A} (l : list A) (acc1 : list A) (acc2 : list A) :=
  match l with
  |[] => (acc1,acc2)
  |hd::[] => (hd::acc1,acc2)
  |hd1::hd2::tl => aux_split tl (hd1::acc1) (hd2::acc2)
  end.

Definition split {A} (l : list A) := aux_split l [] [].

Fixpoint rev_app {A} (l : list A) (acc : list A) := match l with
                                                        |[] => acc
                                                        |hd::tl => rev_app tl (hd::acc)
                                                        end.

Fixpoint max_left_merge {A} (ord : A -> A -> bool) (l1 : list A) (elem : A) (acc : list A) := 
  (* ord must be <= *)
  match l1 with
  |[] => (acc,[])
  |hd1::tl1 => if (ord hd1 elem) 
               then max_left_merge ord tl1 elem (hd1::acc)
               else (acc,l1)
  end.

Fixpoint aux_merge {A} (ord : A -> A -> bool) (l1 : list A) (l2 : list A) (acc : list A):=
  match l2 with
  |[] => rev_app acc l1
  |hd::tl => let (nacc,nl1) := max_left_merge ord l1 hd acc in
             aux_merge ord nl1 tl (hd::acc)
  end.

Definition merge {A} (ord : A -> A -> bool) (l1 : list A) (l2 : list A) := 
  aux_merge ord l1 l2 [].

Fixpoint aux_merge_sort {A} (ord : A -> A -> bool) (l : list A) (n : nat) := 
  match l,n with
  |[],_ => []
  |_::[],_ => l
  |_,O => l
  |_,S n' => let (l1,l2) := split l in
             merge ord (aux_merge_sort ord l1 n') (aux_merge_sort ord l2 n')
  end.

Definition merge_sort {A} (ord : A -> A -> bool) (l : list A) := aux_merge_sort ord l (length l).

Fixpoint aux_powerset {A} (l : list A) (taken : list A) := 
  match l with
  |[] => [taken]
  |hd::tl => rev_app (aux_powerset tl taken) (aux_powerset tl (hd::taken))
  end.

Definition powerset {A} (l : list A) := aux_powerset l [].

Definition mem {A} (eqb : A -> A -> bool) (l : list A) (a : A) := 
  my_fold_left (fun b x => b || (eqb a x)) false l.

