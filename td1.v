Require Import Arith.
Open Scope bool_scope.
Set Universe Polymorphism.
Definition compose A B C (f : B -> C) (g : A -> B) := (fun x:A => f (g x)).

Print compose.

Check (compose nat nat nat S pred).

Print nat.

Definition mybool : Type := forall X : Type, X -> X -> X.
Definition myfalse : mybool := fun (X:Type) (x : X) (y : X) => y.
Definition mytrue : mybool := fun (X:Type) (x : X) (y : X) => x.
Definition myif : forall A, mybool -> A -> A -> A :=
  fun (A:Type) (b : mybool) (a : A) (aa : A) => (b A a aa).

Definition church : Type := forall X : Type, (X -> X) -> X -> X.
Definition zero : church := fun (X:Type) f x => x.
Definition one : church := fun (X:Type) f x => (f x).
Definition succ (c:church) : church := fun (X:Type) (f : X -> X) (x : X) => f (c X f x).
Check succ.
Definition plus : church -> church -> church := fun c1 c2 => c1 _ succ c2.
Definition mult : church -> church -> church := fun c1 c2 => c1 _ (plus c2) zero.
Definition pow (c1 : church) (c2 : church) : church := 
  fun (X : Type) (f : X -> X) (x : X) => (c2 (X->X) (c1 X) f) x.
Check S.
Definition church2nat (c : church) := c nat S O.
Fixpoint nat2church (n : nat) := match n with
                                 |O => zero
                                 |S m => succ (nat2church m)
                                 end.
Definition checktauto (f : bool -> bool) := (f true) && (f false).
Definition checktauto2 (f : bool -> bool -> bool) := checktauto (fun x => checktauto (f x)).
Definition checktauto3 (f : bool -> bool -> bool -> bool) := 
  checktauto (fun x => checktauto2 (f x)). 

Compute (checktauto3 (fun a b c => a || b || c || negb (a && b) || negb (a && c))).
